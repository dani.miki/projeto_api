'use strict'

require('dotenv').config()
const path = require('path')

const users = require(path.join(__dirname, 'functions', 'users'))
const logs = require(path.join(__dirname, 'functions', 'logs'))
const likes = require(path.join(__dirname, 'functions', 'likes'))
const posts = require(path.join(__dirname, 'functions', 'posts'))
const comments = require(path.join(__dirname, 'functions', 'comments'))

module.exports = {
  users: (event, context) => {
    if (event.resource === '/users' && event.httpMethod === 'GET') return users.get(event, context)
    if (event.resource === '/users' && event.httpMethod === 'POST') return users.post(event, context)
    if (event.resource === '/users' && event.httpMethod === 'PUT') return users.put(event, context)
    if (event.resource === '/users' && event.httpMethod === 'DELETE') return users.remove(event, context)
    if (event.resource === '/users/{id}' && event.httpMethod === 'GET') return users.get(event, context)
  },
  logs: (event, context) => {
    if (event.resource === '/logs' && event.httpMethod === 'GET') return logs.get(event, context)
    if (event.resource === '/logs' && event.httpMethod === 'POST') return logs.post(event, context)
    if (event.resource === '/logs' && event.httpMethod === 'PUT') return logs.put(event, context)
    if (event.resource === '/logs' && event.httpMethod === 'DELETE') return logs.remove(event, context)
    if (event.resource === '/logs/{id}' && event.httpMethod === 'GET') return logs.get(event, context)
  },
  likes: (event, context) => {
    if (event.resource === '/likes' && event.httpMethod === 'GET') return likes.get(event, context)
    if (event.resource === '/likes' && event.httpMethod === 'POST') return likes.post(event, context)
    if (event.resource === '/likes/{id}' && event.httpMethod === 'GET') return likes.get(event, context)
  },
  posts: (event, context) => {
    if (event.resource === '/posts' && event.httpMethod === 'GET') return posts.get(event, context)
    if (event.resource === '/posts' && event.httpMethod === 'POST') return posts.post(event, context)
    if (event.resource === '/posts' && event.httpMethod === 'PUT') return posts.put(event, context)
    if (event.resource === '/posts' && event.httpMethod === 'DELETE') return posts.remove(event, context)
    if (event.resource === '/posts/{id}' && event.httpMethod === 'GET') return posts.get(event, context)
  },
  comments: (event, context) => {
    if (event.resource === '/posts' && event.httpMethod === 'GET') return comments.get(event, context)
    if (event.resource === '/posts' && event.httpMethod === 'POST') return comments.post(event, context)
    if (event.resource === '/posts/{id}' && event.httpMethod === 'GET') return comments.get(event, context)
  }
}
